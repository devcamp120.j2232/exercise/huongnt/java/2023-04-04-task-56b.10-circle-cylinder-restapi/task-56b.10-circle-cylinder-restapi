package com.devcamp.resapi.models;

public class Cylinder extends Circle {
    double height = 1.0;


    //khởi tạo phương thức
    public Cylinder() {
    }


    public Cylinder(double radius) {
        super(radius);
    }


    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }


    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }


    public double getHeight() {
        return height;
    }


    public void setHeight(double height) {
        this.height = height;
    }

    //phương thức khác
    public double getVolume (){
        return (4.0 / 3.0 * Math.PI * Math.pow(radius, 3));

    }
}
