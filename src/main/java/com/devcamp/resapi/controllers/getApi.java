package com.devcamp.resapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.resapi.models.Circle;
import com.devcamp.resapi.models.Cylinder;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class getApi {
    @GetMapping("/circle-area")
    public double circleArea() {
        Circle circle1 = new Circle(2.0);
        // in ra console
        System.out.println(circle1.toString());
        return circle1.getArea();

    }

    @GetMapping("/circle-cylinder")
    public double circleCylinder() {
        Cylinder cylinder1 = new Cylinder(2.0, 3.0);
        // in ra console
        System.out.println(cylinder1.toString());
        return cylinder1.getVolume();
    }
}
